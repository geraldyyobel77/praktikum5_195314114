/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas1;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author lenovo
 */
public class tugas1 extends JFrame implements ActionListener {

    private JLabel panjang_JLabel;
    private JLabel lebar_JLabel;
    private JLabel luas_JLabel;
    private JTextField panjangText;
    private JTextField lebarText;
    private JTextField luasText;
    private JButton click;
    
    public tugas1() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setSize(350, 210);
        setTitle("Luas Tanah");
        setLocation(150, 250);
        setResizable(false);

        panjang_JLabel = new JLabel("Panjang (m)");
        panjang_JLabel.setBounds(20, 15, 80, 30);
        contentPane.add(panjang_JLabel);

        lebar_JLabel = new JLabel("Lebar (m)");
        lebar_JLabel.setBounds(20, 50, 80, 30);
        contentPane.add(lebar_JLabel);

        luas_JLabel = new JLabel("Luas (m2)");
        luas_JLabel.setBounds(20, 85, 80, 30);
        contentPane.add(luas_JLabel);

        panjangText = new JTextField();
        panjangText.setBounds(140, 15, 150, 20);
        contentPane.add(panjangText);

        lebarText = new JTextField();
        lebarText.setBounds(140, 50, 150, 20);
        contentPane.add(lebarText);

        luasText = new JTextField();
        luasText.setBounds(140, 85, 150, 20);
        contentPane.add(luasText);

        click = new JButton("Hitung");
        click.setBounds(140, 120, 80, 30);
        click.addActionListener(this);
        contentPane.add(click);
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

     int n1,n2,luas;
     
     try{
         n1=Integer.parseInt(panjangText.getText());
         n2=Integer.parseInt(lebarText.getText());
         luas=n1*n2;
         luasText.setText(Integer.toString(luas));
     }catch(Exception aiueo){
         JOptionPane.showMessageDialog(null, "Maaf, hanya integer yang di perbolehkan","",JOptionPane.ERROR_MESSAGE);
         panjangText.setText("");
         lebarText.setText("");
     }
        setVisible(true);
        setLocation(480, 460);
    }
    public static void main(String[] args) {
        tugas1 frame = new tugas1();
        frame.setVisible(true);
    }

}
